import React from 'react'
import { Field, ErrorMessage } from 'formik'
import ErrorText from '../ErrorText'

const Input = (props) => {
    const {label, name, placeholder, control = 'input', type ='text', ...rest } = props
    return (
        <div className='form-control'>
            <label htmlFor={name}>{label}</label>
            <Field className='form-field' control={control} name={name}
             placeholder={placeholder} type={type} {...rest}/>
            <ErrorMessage name={name} component={ErrorText} />
        </div>
    )
}

export default Input

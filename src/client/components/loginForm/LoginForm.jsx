import React ,{useState}from 'react'
import axios from "axios";
import { Formik, Form } from 'formik'
import Input from '../../../shared/Input'
import {fields, initialValue} from './fields'
import {validationSchema} from '../../validation/validationLoginForm'
import './style/styleForm.scss'
import authToken from "../../authToken/authToken";
import {useDispatch} from "react-redux";
import {isOpenTo} from "../../../app/store/auth/actions/actionAuth";
import {useSelector} from "react-redux";
import { useHistory } from 'react-router-dom'

export const LoginForm = () => {
    const [passwordShown, setPasswordShown] = useState(false);
    const history = useHistory()
    const dispatch = useDispatch();
    const state = useSelector(state => state.isOpen.flag);
    console.log(state)
    const onSubmit = ({loginOrEmail, password}, formProps) =>{
        const request = async ()=>{
            try {
                const respAuth = await axios.post('https://glacial-river-19645.herokuapp.com/api/customers/login', {loginOrEmail, password},
                    {headers: {'Content-Type': 'application/json' }} )
                authToken(respAuth.data.token)
                dispatch(isOpenTo())
                history.push('/products')
            } catch (error){
                alert(error.response.data.loginOrEmail)
                console.dir(error)
            }
        }
        request()
        formProps.resetForm()
    }
    const {loginOrEmail, password} = fields

    return (
        <Formik 
        initialValues={initialValue}
        validationSchema={validationSchema}
        onSubmit={onSubmit}>
            {formik =>{         
                return(
                <Form className ='form login-form' >
                    <Input {...loginOrEmail} />
                    <Input {...password} type={passwordShown ? "text" : "password"} />                    
                    <div>
                        <button className='buttonAuth' type='submit' disabled={!formik.isValid}>Войти</button>
                    </div>
                </Form>
                )
            }}                               
        </Formik>
    )
}

export default LoginForm

export const fields = {
    loginOrEmail:{
        name: 'loginOrEmail',
        label: 'Логин',
        placeholder: 'Введите логин',
        type: 'text',
        className: 'label'
    },
    password: {
        name: 'password',
        label: 'Пароль',
        placeholder: 'Введите пароль',
        type: 'password',
        className: 'label'
    },
}

export const initialValue = {
    loginOrEmail: '',
    password: ''
}
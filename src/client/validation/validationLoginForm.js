import * as Yup from 'yup'

export const validationSchema = Yup.object({
    password: Yup.string()
    .min(5, 'Too Short!')
    .max(32, 'Too Long!')
    .required('Required'),
    loginOrEmail: Yup.string().required('Required'),
})
import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import {useSelector} from "react-redux";

const PrivateRoute = ({component: Component, ...rest}) => {
    const state = useSelector(state => state.isOpen.flag)

    return (
        <Route {...rest} render={(props) => state ? <Component {...props}/> : <Redirect to='/' />} />
    );
};

export default PrivateRoute;
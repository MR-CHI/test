import React from 'react';
import './header-style.scss';
import Navbar from "../components/NavBar/Navbar";
import {useSelector} from "react-redux";
import {NavLink} from "react-router-dom";
import {useDispatch} from "react-redux";
import {isClose} from "../../../app/store/auth/actions/actionUnAuth";


const Header = () => {
    const show = useSelector(state => state.isOpen.flag);
    const dispatch = useDispatch()


    return (
        <section  className='header-container'>
            <div className='header-container_navigator'>
                <div className='header-container_navigator__left'>
                    <div className='logo'>Логотип</div>
                    {show ?  <NavLink to={`/products`} className='logIn'>Продукция</NavLink> : <Navbar className='liStyle' classNameLink='logIn' />}
                </div>
                <p onClick={()=> dispatch(isClose())} className='logIn'>{show ? <NavLink to={`/logIn`} className='logIn'> Выйти из аккаунта</NavLink> : null}</p>
            </div>
        </section>
    )
}
export default Header

import React from "react";
import {linkProps} from "./linkProps";
import {NavLink} from "react-router-dom";
import {v4 as uuidv4} from 'uuid'


const Navbar = ({className, classNameLink}) => {
    const linksElements = linkProps.map(({to, text}) =>
        <li  key={uuidv4()} className={className}>
            <NavLink to={to}  className={classNameLink}>{text}</NavLink>
        </li>)
    return (
             <>
               {linksElements}
             </>)
}

export default Navbar

import React from 'react';
import {useSelector} from "react-redux";
import './oneProductStyle.scss'
import '../../pages/ProductsPage/productStyle/productStyle.scss'



const OneProduct = ({match}) =>{
    const {params: {oneProduct},} = match;
    const dataProduct = useSelector(state => state.products.products).filter(item =>  item.SKU === oneProduct);

    return(
        <div className='wrapOn' >
            <h4  className='titleProduct'>Товар</h4>
            <div className='containerOneProduct'>
                <div style={{border: '1px solid #e3e3e3', width: '601px', height: '461px'}}>
                    <img className='oneProductImage' src={dataProduct[0].ImageURL} alt=""/>
                </div>
                <div style={{margin: '-17px 0 0 30px'}}>
                    <p>Роутер</p>
                    <div style={{fontWeight: 'bold', fontSize: '30px'}}>{dataProduct[0].SKU}</div>
                    <p>Количество товаров: {dataProduct[0].TotalStock}</p>
                    <p style={{color: '#008000', fontWeight: 'bold'}}>{dataProduct[0].RetailPrice} грн</p>
                    <p>{dataProduct[0].Description}</p>
                </div>
            </div>




        </div>
    )
}
export default OneProduct
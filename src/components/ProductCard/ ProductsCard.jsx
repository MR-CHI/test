import React from 'react';

const ProductsCard = ({clCard, imgClassName, src, alt, sku, price}) =>{
    return(
        <div className={clCard}>
            <img className={imgClassName} src={src} alt={alt}/>
            <div style={{display: 'flex', justifyContent: 'space-between', width: '360px', marginTop: '15px'}}>
                <div >Роутер
                    <p style={{fontWeight: 'bold' }}>{sku}</p>
                </div>
                <div style={{color: '#008000', fontWeight: 'bold'}}>{price} грн</div>
            </div>
        </div>

    )

}
export default ProductsCard
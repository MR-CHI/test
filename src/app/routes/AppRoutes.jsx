import React from "react";
import {Switch, Route, Redirect } from 'react-router-dom'
import LoginForm from "../../client/components/loginForm/LoginForm";
import PrivateRoute from "../../components/PrivatePoute/PrivateRoute";
import ProductPage from "../../pages/ProductsPage/ProductsPage";
import OneProduct from "../../components/OneProduct/OneProduct";

const AppRoutes = () => {
    return(
        <Switch>
            <Route exact path='/' >

            </Route>
            <Route exact path='/logIn' >
                <LoginForm />
            </Route>
            <Route exact path='/products/:oneProduct' component={OneProduct} />

            <PrivateRoute component={()=> <ProductPage />} path='/products' />

        </Switch>
    )
}

export default AppRoutes;

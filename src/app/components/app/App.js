import React from 'react';
import {BrowserRouter as Router} from "react-router-dom";
import Header from "../../../components/Header/Header/Header";
import Footer from "../../../components/Footer/Footer";
import Container from "../../../components/Container/Container";
import AppRoutes from "../../routes/AppRoutes";
import {Provider} from "react-redux";
import store from "../../store/store";


function App() {
    return (
        <Provider store={store}>
            <Router>
                <Header />
                <Container>
                    <AppRoutes />
                </Container>
                <Footer />
            </Router>
        </Provider>

    );
}

export default App;
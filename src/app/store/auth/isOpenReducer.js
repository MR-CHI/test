import {IS_OPEN, IS_CLOSE} from "./constants";

const initialState = {
    flag: false
};

export const isOpenReducer = (state = initialState, {type, payload}) =>{
    switch (type) {
        case IS_OPEN :
            return {...state,  flag: true}
        case IS_CLOSE :
            return {...state, flag: false}

            default : return state
    }
}
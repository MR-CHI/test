import {IS_CLOSE} from "../constants";

export const isClose = (payload) =>{
    return {
        type: IS_CLOSE,
        payload
    }
};
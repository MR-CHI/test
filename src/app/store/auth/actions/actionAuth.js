import {IS_OPEN} from "../constants";

export const isOpenTo = (payload) =>{
    return {
        type: IS_OPEN,
        payload
    }
};
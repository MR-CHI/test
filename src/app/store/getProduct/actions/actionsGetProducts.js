import axios from "axios";
import {ADD_PRODUCTS} from "../constants/actionType";

export const actionsGetProducts = () =>{

    const funcGetProducts = async (dispatch) =>{
        try {
            const response = await axios.get("http://localhost:3000/data.json");
            const {products} = response.data;

            dispatch({type: ADD_PRODUCTS, payload: products})

        }catch (error) {
            console.log(error)

        }
    }
    return funcGetProducts;
}
import {createStore, combineReducers, applyMiddleware} from "redux";
import {isOpenReducer} from "./auth/isOpenReducer";
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from "redux-thunk";
import {productsReducer} from "./getProduct/productsReducer";
import {isClose} from "./auth/actions/actionUnAuth";

const rootReducer = combineReducers({
    isOpen: isOpenReducer,
    isClose: isClose,
    products: productsReducer

})

const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)))

export default store
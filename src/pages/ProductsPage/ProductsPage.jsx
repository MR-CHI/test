import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {actionsGetProducts} from "../../app/store/getProduct/actions/actionsGetProducts";
import ProductsCard from "../../components/ProductCard/ ProductsCard";
import './productStyle/productStyle.scss'
import {NavLink} from "react-router-dom";


const ProductPage = () =>{
    const dispatch = useDispatch();
    const allProducts = useSelector(state => state.products.products);
    const result = allProducts.map(({SKU, ImageURL, RetailPrice}) => <NavLink style={{textDecoration: 'none', color: 'black'}} to={`/products/${SKU}`}><ProductsCard
        clCard='containerCardProduct'
        imgClassName='productCardImg'
        sku={SKU}
        price={RetailPrice}
        src={ImageURL}/>
    </NavLink>)


    useEffect(() => {
        const action = actionsGetProducts();
        dispatch(action)
    }, [])

    return(
        <div className='pageProducts'>
            <h4 className='titleProduct'>Наша продукция</h4>
            <div className='containerAllProduct'>
                {result}
            </div>
        </div>

    )
}
export default ProductPage